import typing as tp
import uuid

import json
from abc import ABCMeta


class DomainModel(metaclass=ABCMeta):
    """
    Use only with classes that defined with annotations ::

        class Example(DomainModel):
            name: str
            age: int
    """

    # def __init__(self, **kwargs):
    #     # use UUID as entity identification
    #     self.uuid: uuid.UUID = uuid.uuid4()

    #     #  get all variables passed to class and their types
    #     self.annotations = getattr(self, '__annotations__', {})
    #     self.class_name = self.__class__.__name__
    #     self._initialize_values(kwargs)

    def __annots_post_init__(self, **kwargs):
        # use UUID as entity identification
        self.uuid = str(uuid.uuid4())

        #  get all variables passed to class and their types
        self.annotations = getattr(self, '__annotations__', {})
        self.class_name = self.__class__.__name__
        self._initialize_values(kwargs)

    def __repr__(self):
        mapped = dict(uuid=self.uuid)
        mapped.update(self.to_dict())
        return f"{self.class_name}({mapped})"

    def _initialize_values(self, dct: dict):
        # walk through passed named parameters and add to instance if there valid
        for attr, value in dct.items():
            self._validate(attr, value)
            setattr(self, attr, value)

    def _validate(self, attr, value):
        if attr not in self.annotations:
            raise ValueError(f"The {class_name} has not attr `{attr}`")
        if not type(value) is self.annotations[attr]:
            raise TypeError(f"Not valid type for arg: `{attr}`")

    def to_dict(self) -> dict:
        # return dict of attributes and values
        return {attr: getattr(self, attr) for attr in self.annotations}

    def from_dict(self, dct: dict):
        # initialize object from dict
        self._initialize_values(dct)
