from collections import Sequence

import attr

@attr.s(repr=False, slots=True)
class _ExistsInValidator(object):
    seq = attr.ib()

    def __call__(self, inst, attr, value):
        if value not in self.seq:
            raise ValueError(
                "You had inserted {value!r} value, but available options are {seq!s}."
                .format(seq=self.seq,
                        value=value),
                attr, self.seq, value,
            )

    def __repr__(self):
        return (
            "<exists_in validator for sequnce {seq}>"
            .format(seq=self.seq)
        )

def exists_in(seq):
    return _ExistsInValidator(seq)


@attr.s(repr=False, slots=True)
class _InstancesOfValidator(object):
    type = attr.ib()

    def __call__(self, inst, attr, seq):
        if not hasattr(attr.default, 'factory'):
            raise TypeError(
                "For use this validator you should define the "
                "default factory in the attr: `{}` of the class "
                "`{!r}`".format(attr.name, inst.__class__))

        if not (isinstance(seq, attr.default.factory)):
            raise TypeError("Role should be in sequnce with type: {!r}. "
                            "Got the type: {!r}".format(
                                attr.default.factory,
                                type(seq)))

        for elem in seq:
            if not isinstance(elem, self.type):
                raise TypeError(
                    "'{name}' must be {type!r} (got {value!r} that is a "
                    "{actual!r})."
                    .format(name=attr.name, type=self.type,
                            actual=value.__class__, value=value),
                    attr, self.type, value,
                )

    def __repr__(self):
        return (
            "<instances_of validator for type {type!r} and given sequence"
            .format(type=self.type)
        )

def instances_of(type):
    return _InstancesOfValidator(type)


def unique(instance, attribute, value):
    sequence = getattr(instance, attribute.name)
    if value in sequence:
        raise ValueError("")

__all__ = ['exists_in', 'instances_of']
