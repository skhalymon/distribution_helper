from abc import ABC
from distributionhelper import models as m
from distributionhelper.domain import entity as e

import abc


class AbstractDataMapper(object):
    __metaclass__ = abc.ABCMeta



class BaseRepository(ABC):

    # @abc.abstractmethod
    # def find_all(self):
    #     return

    # @abc.abstractmethod
    # def find_by_id(self, id):
    #     return

    # @abc.abstractmethod
    # def create(self):
    #     return

    # @abc.abstractmethod
    # def update(self):
    #     return

    # @abc.abstractmethod
    # def delete(self):
    #     return

    def _check(self, element, key, value):
        """
        Allow to check is value accept criteria.

        Example ::
            {'size': 93}
            {'size__eq': 93}
            {'size__lt': 60}
            {'size__gt': 400}
        """
        if '__' not in key:
            key = key + '__eq'

        key, operator = key.split('__')

        if operator not in ['eq', 'lt', 'gt']:
            raise ValueError('Operator {} is not supported'.format(operator))

        operator = '__{}__'.format(operator)

        return getattr(element[key], operator)(value)


def to_dict(model_instance, query_instance=None):
    # sqlalchemy helper for convert model to dict
    if hasattr(model_instance, '__table__'):
        return {c.name: str(getattr(model_instance, c.name)) for c in model_instance.__table__.columns}
    elif query_instance:
        cols = query_instance.column_descriptions
        return { cols[i]['name'] : model_instance[i]  for i in range(len(cols)) }


class SQLAlchemyRepository(BaseRepository):

    def __init__(self, session, model):
        self.session = session
        self.model = model

    def to_dict(self, model_instance, query_instance=None):
        # sqlalchemy helper for convert model to dict
        if hasattr(model_instance, '__table__'):
            return {c.name: str(getattr(model_instance, c.name)) for c in model_instance.__table__.columns}
        elif query_instance:
            cols = query_instance.column_descriptions
            return {cols[i]['name']: model_instance[i]
                    for i in range(len(cols))}
        else:
            raise ValueError("You pass not correct values")

    def from_dict(self, entity_instance, model=None):
        if model is None:
            model = self.model
        model_instance = model(**entity_instance.to_dict())


# class AccountRepository(SQLAlchemyRepository):

#     def __init__(self, session):
#         self.session = session

#     def create(self, account):
#         account = m.Account(username, password, social_networks)
#         self.dbsession.add(account)
#         self.dbsession.flush()
#         return self._map_to_entity(account)

#     def get_account_by_id(self, id):
#         account = self.dbsession.query(m.Account).filter_by(id=id)
#         return self._map_to_entity(account)

#     def _map_to_entity(self, instance):
#         return e.Account(instance.id, instance.username,
#                          instance.password, instance.social_networks)


# class PickleStorage:
#   def __init__(self, path):
#     self.path = path

#   def open_file(self, filename, mode):
#     return open(os.path.join(self.path, filename), mode)

#   def get(self, filename):
#     with self.open_file(filename, "rb") as f:
#         obj = pickle.load(f)
#         return obj

#   def store(self, obj, filename):
#     with self.open_file(filname, "wb") as f:
#         pickle.dump(obj, f)

# class AccountRepository:
#   def __init__(self, storage):
#     self.storage = storage

#   def get(self, account_name):
#     return self.storage.get("account-%s" % account_name)

#   def store(self, account):
#     self.storage.store(account, "account-%s" % account.name)