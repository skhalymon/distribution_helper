from zope.interface import Interface


class ISocialSearch(Interface):
    """
        Search for people and groups by criterias
    """

    def read_users_from_(self, url: str, msg_open_check: bool) -> list:
        """
        Return
        :param url: search url address to group where users present to parse
        :param msg_open_check: check if can write message
        :param is_friend_list: if friend list, change method of getting address to write msg
        :return: list of users address
        """

    def read_groups(self, url: str) -> list:
        """
        Return
        :param url: search url address where groups present to parse
        :return: list of users address
        """


class ISocial(Interface):
    """
        General interface
    """
    def _captcha_procces(self):
        """
        Solving captcha
        """

    def is_captcha(self) -> bool:
        """
        Check if captcha present on current page
        """

    def is_message_sent(self, n: int) -> bool:
        """
        Check if message sent to user. Sleep half second after repeat.
        :param n: Number of check reps
        """

    def send_message_to(self, user):
        """
            Send message to specific user
        """

    def login(self):
        """
        Login process
        """

    def logout(self):
        """
        Logout process
        """
