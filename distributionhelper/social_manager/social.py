import time

from zope.interface import implementer

from selenium.common.exceptions import (NoSuchElementException,
                                        WebDriverException,
                                        ElementNotVisibleException)
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from splinter import Browser

from distributionhelper.social_manager.interfaces import (
    ISocial)

import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


@implementer(ISocial)
class SocialVK:
    def __init__(self, browser: Browser, username: str, password: str):
        self.browser = browser
        self.wd = self.browser.driver
        self.username = username
        self.password = password

    def __str__(self):
        return f"<SocialVK> username={self.username}"

    def login(self, url='https://vk.com/'):
        self.browser.visit(url)
        self.browser.execute_script("document.querySelectorAll('#index_email')[0].click()")
        user_input = self.browser.find_element_by_xpath('//*[@id="index_email"]')
        user_input.fill(self.username)
        self.browser.execute_script("document.querySelectorAll('#index_pass')[0].click()")
        password_input = self.browser.find_element_by_xpath('//*[@id="index_pass"]')
        password_input.fill(self.password)
        login_button = self.browser.find_element_by_xpath('//*[@id="index_login_button"]')
        login_button.click()
        logger.info('Logged in')

    def write_msg(self, msg):
        try:
            message_field = self.browser.find_by_css('.im_editable')
            message_field.fill(msg)
        except NoSuchElementException as e:
            logger.error('NoSuchElementException on ', e)
            time.sleep(2)
            logger.info('Trying to find msg field again')
            self.write_msg(msg)

    def send_message_to_(self, msg_url, msg):
        self.browser.visit(msg_url)
        print('Select peoples to send msg')
        self.manual_actions()
        self.write_msg(msg)
        if self.is_message_sent():
            logger.info('Msg success sent ({})'.format(msg_url))
            return True
        else:
            logger.info("Msg didn't sent! ({})".format(msg_url))
            return False

    def _captcha_procces(self):
        box_layout = self.browser.find_by_css('.box_layout')
        captcha = box_layout.find_by_css('.captcha')
        captcha_msg = input('Write captcha below:\n')
        captcha_input = captcha.find_by_css('.big_text')
        captcha_input.fill(captcha_msg)
        submit = box_layout.find_by_css('.flat_button')
        submit.click()

    def is_captcha(self):
        try:
            self.browser.find_by_css('.captcha')
            return True
        except NoSuchElementException:
            return False


@implementer(ISocial)
class SocialYouTube:
    def __init__(self, username: str, password: str):
        self.browser: Browser = None
        self.logged_in = False
        self.username = username
        self.password = password

    def __str__(self):
        return f"<SocialYouTube> username={self.username}"

    def login(self, url='https://www.youtube.com/signin'):
        self.browser.visit(url)
        self.browser.find_by_id('Email').fill(self.username)
        self.browser.click_link_by_id('next')
        # WebDriverWait(self.wd, 10).until(EC.presence_of_element_located((By.ID, 'Passwd')))
        self.browser.find_by_id('Passwd').fill(self.password)
        self.browser.click_link_by_id('signIn')
        logger.info('User <{}> logged in'.format(self.username))

    def watch_video(self, url=None, mute_audio=True, video_duration=None):
        if url:
            self.browser.visit(url)

        if not video_duration:
            video_duration = self.browser.evaluate_script("document.getElementById('movie_player').getDuration();")

        current_time = lambda: self.browser.evaluate_script(
            "document.getElementById('movie_player').getCurrentTime();")

        if mute_audio:
            self.browser.execute_script("document.getElementById('movie_player').mute();")

        while current_time() <= video_duration / 2:
            time.sleep(10)
        else:
            logger.info('Video {} with duration {} watched'.format(url, video_duration))
            return True

    def watch_and_like_video(self, url):
        self.browser.visit(url)
        watched = self.watch_video(url)
        liked = self._like_video()
        logger.info('Video <{}> watched: {} and liked: {}'.format(url, watched, liked))
        return liked

    def _is_video_liked(self):
        liked = self.browser.find_element_by_xpath('//*[@id="watch8-sentiment-actions"]/span/span[2]/button')

        if liked:
            return True

        return False

    def _like_video(self):
        try:
            like_btn = self.browser.find_by_xpath('//*[@id="watch8-sentiment-actions"]/span/span[1]/button')
            like_btn.click()
            logger.info('Clicked on like button')
            return True
        except ElementNotVisibleException:
            logger.warning('ElementNotVisibleException on page')
            if self._is_video_liked():
                return True
            return False
