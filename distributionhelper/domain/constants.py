import enum


class UserRoles(enum.Enum):
    admin = 'Admin'
    organizer = 'Organizer'
    moderator = 'Moderator'
    volunteer = 'Volunteer'


class AttachementTypes(enum.Enum):
    photo = 'Photo'
    video = 'Video'
    audio = 'Audio'


class SocialNetworks(enum.Enum):
    vk = 'Vkontakte'
    fb = 'Facebook'
    youtube = 'Youtube'


class WorkerAvailabilityStatuses(enum.Enum):
    free = 'Worker is free'
    busy = 'Worker is busy'


class WorkerType(enum.Enum):
    browser = 'Browser worker'
    volunteer = 'Volunteer worker'


class AvailableBrowsers(enum.Enum):
    chrome = 'Chromium'
    firefox = 'Firefox'
    phantomjs = 'Phantomjs'


class MessageDeliveryStatuses(enum.Enum):
    sent = 'Sent'
    delivered = 'Delivered'
    pending = 'Pending'


class CampaignStatuses(enum.Enum):
    draft = 'Draft'
    pending = 'Waiting for approve'
    approved = 'Approved'
    running = 'Running'
    error = 'Error occurred during campaign execution'
    finished = 'Finished'
