
import typing as tp
from urllib.parse import urlsplit

from zope.interface import implementer
from splinter import Browser

from distributionhelper.social_manager.interfaces import ISocial
from distributionhelper.worker.interfaces import IBrowserWorker, AlreadyInitialized, MustBeInitializedFirst


import logging
logger = logging.getLogger(__name__)


@implementer(IBrowserWorker)
class BrowserWorker:
    def __init__(self, social: ISocial):
        """
        :param social: not initialized object of requested social network
        """
        self.social = social
        self.browser = None
        self._webdriver_initialized = False

    def initialize_webdriver(self, browser_type, **kwargs):
        if self._webdriver_initialized:
            raise AlreadyInitialized
        self.browser = Browser(browser_type, **kwargs)
        self.social.browser = self.browser
        self._webdriver_initialized = True
        return self

    def shutdown_webdriver(self):
        self.browser.driver.quit()

    def get_webdriver_url(self) -> str:
        return self.browser.driver.command_executor._url

    def get_cookies(self) -> tp.List[dict]:
        cookies = self.browser.driver.get_cookies()
        url = self.browser.driver.current_url
        domain = urlsplit(url).netloc
        cookies = [dict(cookie, domain=domain) for cookie in cookies]
        return cookies

    def set_cookies(self, cookies):
        for cookie in cookies:
            self.browser.driver.add_cookie(cookie)

