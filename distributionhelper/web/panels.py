from pyramid_layout.panel import panel_config

from distributionhelper.models import Campaign, Recipient
from distributionhelper.domain.constants import CampaignStatuses

from . import admins


@panel_config(name='admin_panel', context=admins.CampaignAdmin, renderer='admin/campaign_progress_panel.html')
def campaigns_progress(context, request, **kwargs):
    """Admin panel for Campaign."""

    dbsession = request.dbsession
    running_campaigns = dbsession.query(Campaign).filter(Campaign.status == CampaignStatuses.running)
    count = dbsession.query(Campaign.id).count()
    model_admin = context
    title = model_admin.title
    return dict(locals(), **kwargs)


@panel_config(name='admin_panel', context=admins.RecipientAdmin, renderer='admin/recipient_panel.html')
def recipients(context, request, **kwargs):
    """Admin panel for Recipient."""

    dbsession = request.dbsession
    count = dbsession.query(Recipient.id).count()
    model_admin = context
    title = model_admin.title
    return dict(locals(), **kwargs)

