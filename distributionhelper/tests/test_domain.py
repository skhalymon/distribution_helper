from distributionhelper.domain import Attachement, Message, Tag
from distributionhelper.domain import Account
from distributionhelper.domain import Campaign
from distributionhelper.domain import Recipient
from distributionhelper.domain import SocialNetwork, SocialNetworkAccess
from distributionhelper.domain import User
from distributionhelper.domain import BrowserWorker


def test_worker_init(worker_factory):
    assert isinstance(worker_factory(), BrowserWorker)


def test_user_init(user_factory):
    assert isinstance(user_factory(), User)


def test_social_network_init(social_network_factory):
    assert isinstance(social_network_factory(), SocialNetwork)


def test_social_network_access_init(social_network_access_factory):
    assert isinstance(social_network_access_factory(), SocialNetworkAccess)


def test_recipient_init(recipient_factory):
    assert isinstance(recipient_factory(), Recipient)


def test_campaign_init(campaign_factory):
    assert isinstance(campaign_factory(), Campaign)


def test_account_init(account_factory):
    assert isinstance(account_factory(), Account)


def test_atachement_init(attachement_factory):
    assert isinstance(attachement_factory(), Attachement)


def test_message_init(message_factory):
    assert isinstance(message_factory(), Message)


def test_tag_init(tag_factory):
    assert isinstance(tag_factory(), Tag)
