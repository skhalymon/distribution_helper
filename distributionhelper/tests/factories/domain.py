import random

import factory
from pytest_factoryboy import register

from distributionhelper.domain import *
from distributionhelper.domain.constants import *


@register
class UserFactory(factory.Factory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    password = factory.Faker('password')
    email = factory.Faker('email')

    class Meta:
        model = User


@register
class SocialNetworkFactory(factory.Factory):
    name = factory.Faker('name')
    login_url = factory.Faker('url')

    class Meta:
        model = SocialNetwork


@register
class AccountFactory(factory.Factory):
    username = factory.Faker('name')
    password = factory.Faker('password')
    profile_url = factory.Faker('url')
    social_networks = [factory.SubFactory(SocialNetworkFactory) for i in range(2)]

    class Meta:
        model = Account


@register
class SocialNetworkAccessFactory(factory.Factory):
    social_network = factory.SubFactory(SocialNetworkFactory)
    account = factory.SubFactory(AccountFactory)
    cookies = []

    class Meta:
        model = SocialNetworkAccess


@register
class RecipientFactory(factory.Factory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    social_network = factory.SubFactory(SocialNetworkFactory)
    profile_url = factory.Faker('url')
    avatar_url = factory.Faker('url')

    class Meta:
        model = Recipient


@register
class CampaignFactory(factory.Factory):
    name = factory.Faker('name')

    class Meta:
        model = Campaign


@register
class TagFactory(factory.Factory):
    name = factory.Faker('name')

    class Meta:
        model = Tag


@register
class AttachementFactory(factory.Factory):
    # kind = random.choice(ATTACHEMENTS_TYPE)
    kind = 'photo'
    body = factory.Faker('text')

    class Meta:
        model = Attachement


@register
class MessageFactory(factory.Factory):
    text = factory.Faker('text')

    class Meta:
        model = Message

@register
class WorkerFactory(factory.Factory):
    browser = 'chrome'
    avalability = 'free'
    # avalability = random.choice(AVALABILITY_STATUSES)
    ipv4_address = factory.Faker('ipv4')

    class Meta:
        model = BrowserWorker
