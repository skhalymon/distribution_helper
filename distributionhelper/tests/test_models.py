from distributionhelper.models import Message
from distributionhelper.models import Account
from distributionhelper.models import Campaign
from distributionhelper.models import Recipient
from distributionhelper.models import User
from distributionhelper.models import Worker

#
# def test_worker_init(worker_factory):
#     assert isinstance(worker_factory(), Worker)

def test_recipient_init(recipient_factory):
    assert isinstance(recipient_factory(), Recipient)
#
#
# def test_campaign_init(campaign_factory):
#     assert isinstance(campaign_factory(), Campaign)
#
#
# def test_account_init(account_factory):
#     assert isinstance(account_factory(), Account)
#
#
# def test_message_init(message_factory):
#     assert isinstance(message_factory(), Message)
